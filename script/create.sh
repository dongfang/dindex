#! /bin/bash

TOT=$1
if [ -n TOT ]; then
	TOT=10
fi

for (( cnt=0; cnt<$TOT; cnt++ )); do
	touch fusion_mount/f_$cnt
done
