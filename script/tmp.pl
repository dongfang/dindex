#!/home/dongfang/software/perl/bin/perl

use Time::HiRes qw/ time sleep /;
use feature qw(say);

#program name:
#say $0; 

$TOT = $ARGV[0];
if ("" == $TOT) 
{
	$TOT = 100;
}

$start=time;
#say "start = $start";

#system('sleep 2');
for ($cnt = 0; $cnt < $TOT; $cnt++)
{
	#print "$i \n";
	$cmd = "touch fusion_mount/f_$cnt";
	#say $cmd;
	system($cmd);
}

$end=time;
#say "end = $end";

say "Walltime (ms) = ", 1000 * ($end-$start);
